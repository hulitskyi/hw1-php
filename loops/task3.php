<?php

function squareRoot($root) {
    for ($i = 1; ; $i++) {
        if ($root == $i * $i) {
            return $i;
        }
        if ($root < $i * $i) {
            return $i;
        }
    }
}

function squareRootBinary($root) {
    $left = 1;
    $right = $root;
    $mid = 0;
    $prev = 0;
    for (; ;) {
        $mid = ceil(($left + $right) / 2);
        if ($prev == $mid) {
            break;
        }
        $key = abs($mid * $mid);

        if ($root == $key) {
            return $mid;
        }
        if ($key > $root) {
            $right = $mid;
        } else {
            $left = $mid;
        }
        $prev = $mid;
    }
    if ($root != $mid * $mid) {
        return $mid;
    }
}

echo ("Метод последовательного подбора:");echo ('<br>');
echo ("число: 30, корень:"); echo squareRoot(30);echo ('<br>');
echo ("число: 25, корень:"); echo squareRoot(25);echo ('<br>');
echo ("число: 109, корень:"); echo squareRoot(109);echo ('<br>');
echo ("Метод бинарного поиска:");echo ('<br>');
echo ("число: 30, корень:"); echo squareRootBinary(30);echo ('<br>');
echo ("число: 25, корень:"); echo squareRootBinary(25);echo ('<br>');
echo ("число: 109, корень:"); echo squareRootBinary(109);echo ('<br>');
echo '<a href="index.php">return</a>';echo ('<br>');