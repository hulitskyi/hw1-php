<?php

function sumOfDigits($num) {
    $sum = 0;
    $n = strval($num);
    for($i = 0; $i < strlen($n); $i++) {
        if($i == 0 && $n[$i] == '-') {
            continue;
        }
        else {
            $sum += intval($n[$i]);
        }
    }
    return $sum;
}

echo ("Сумма цифр числа 125: "); echo sumOfDigits(125); echo ("<br>");
echo ("Сумма цифр числа -555: "); echo sumOfDigits(-555); echo ("<br>");
echo ("Сумма цифр числа 123: "); echo sumOfDigits(123); echo ("<br>");
echo '<a href="index.php">return</a>';echo ('<br>');