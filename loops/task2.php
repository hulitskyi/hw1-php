<?php

function isPrimeNumber($num){
    $test = false;
    for ($i = 2; $i <= $num; $i++) {
        if ($num % $i === 0 && $i !== $num) {
            $test = false;
            break;
        } else {
            $test = true;
        }
    }
    if ($test) {
        return "Число $num: простое";
    } else {
        return "Число $num: не простое";
    }
}

echo isPrimeNumber(10);echo ('<br>');
echo isPrimeNumber(11);echo ('<br>');
echo isPrimeNumber(109);echo ('<br>');
echo isPrimeNumber(111);echo ('<br>');
echo '<a href="index.php">return</a>';echo ('<br>');