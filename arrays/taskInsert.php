<?php

function insertSort($arr) {
    for ($i = 1; $i < sizeof($arr); $i++) {
        $key = $arr[$i];
        $j = $i - 1;
        while ($j >= 0 && $arr[$j] > $key) {
            $arr[$j + 1] = $arr[$j];
            $j = $j - 1;
        }
        $arr[$j + 1] = $key;
    }
    return $arr;
}

echo ("insertSort");echo ('<br>');
echo (json_encode(insertSort([6,3,4,6,8,3,2,1])));echo ('<br>');
echo '<a href="index.php">return</a>'; echo ('<br>');