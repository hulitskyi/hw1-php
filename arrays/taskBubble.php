<?php

function bubbleSort($arr) {
    for ($i = 0; $i < sizeof($arr); $i++) {
        for ($j = 0; $j < sizeof($arr) - $i - 1; $j++) {
            if ($arr[$j] > $arr[$j + 1]) {
                $temp = $arr[$j];
                $arr[$j] = $arr[$j + 1];
                $arr[$j + 1] = $temp;
            }
        }
    }
    return $arr;
}

echo ("bubbleSort");echo ("<br>");
echo (json_encode(bubbleSort([6,3,4,6,8,3,2,1])));echo ("<br>");
echo '<a href="index.php">return</a>'; echo ('<br>');