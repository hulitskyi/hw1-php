<?php

function selectSort($arr) {
    for ($i = 0; $i < sizeof($arr); $i++) {
        $min = $i;
        for ($j = $i + 1; $j < sizeof($arr); $j++) {
            if ($arr[$j] < $arr[$min]) {
                $min = $j;
            }
        }
        $temp = $arr[$min];
        $arr[$min] = $arr[$i];
        $arr[$i] = $temp;
    }
    return $arr;
}

echo ("selectSort");echo ('<br>');
echo (json_encode(selectSort([6,3,4,6,8,3,2,1]))); echo ('<br>');
echo '<a href="index.php">return</a>'; echo ('<br>');