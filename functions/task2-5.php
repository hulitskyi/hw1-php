<?php

$numto19 = ["one " , "two ", "three " , "four ", "five ", "six ", "seven ", "eight ", "nine ", "ten ", "eleven ",
    "twelve ", "thirteen ","fourteen ","fifteen ","sixteen ","seventeen ","eighteen ","nineteen "];
$tens = [ "twenty ", "thirty ", "forty ", "fifty ", "sixty ", "seventy ", "eighty ", "ninety "];
$size = ["hundred ", "thousand ", "million ", "billion "];

function numToString($num) {
    global $numto19, $tens, $size;
    $strnum = "";
    $mod = $num;

    for ($i = 1000000000; $i >= 1; $i /= 1000) {

        $div = floor($mod / $i);

        if ($div == 0) {
            continue;
        } else {
            if ($div >= 100) {
                $strnum .= $numto19[intval($div / 100) - 1];
                $strnum .= $size[0];
                $div %= 100;
            }
            if ($div >= 20) {
                $strnum .= $tens[intval($div / 10) - 2];
                if ($div % 10 !== 0) {
                    $strnum .= $numto19[intval($div % 10) - 1];
                }
            } else if ($div <= 19 && $div !== 0) {
                $strnum .= $numto19[$div - 1];
            }
        }
        $mod %= $i;

        $strnum .= sizeNum($i);

    }
    return $strnum;
}

function sizeNum($length)
{
    global $size;
    if($length == 1000000000)
    {return $size[3];}
    if($length == 1000000)
    {return $size[2];}
    if($length == 1000)
    {return $size[1];}
    else
        return "";
}
echo numToString(90156589);echo ('<br>');
echo '<a href="index.php">return</a>'; echo ('<br>');