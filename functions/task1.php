<?php

function dayOfWeek($day) {
    switch($day){
        case 1:
            return "Понедельник";
        case 2:
            return "Вторник";
        case 3:
            return "Среда";
        case 4:
            return "Четверг";
        case 5:
            return "Пятница";
        case 6:
            return "Суббота";
        case 7:
            return "Воскресенье";
        default:
            return null;
    }
}

echo ("dayOfWeek"); echo ('<br>');
echo (dayOfWeek(2)); echo ('<br>');
echo (dayOfWeek(5)); echo ('<br>');
echo (dayOfWeek(-2)); echo ('<br>');
echo '<a href="index.php">return</a>'; echo ('<br>');