<?php

function quaterOfPoint($x, $y) {
    if ($x === 0 && $y === 0) {
        return "Точка лежит в центре координат";
    } else if ($x === 0) {
        return "Точка лежит на оси Y";
    } else if ($y === 0) {
        return "Точка лежит на оси X";
    } else if ($x > 0) {
        if ($y > 0) {
            return "Точка лежит в I четверти";
        } else {
            return "Точка лежит в IV четверти";
        }
    }
    if ($y > 0) {
        return "Точка лежит в II четверти";
    } else {
        return "Точка лежит в III четверти";
    }
}

echo quaterOfPoint(5,2);echo ('<br>');
echo quaterOfPoint(0,2);echo ('<br>');
echo quaterOfPoint(9,0);echo ('<br>');
echo quaterOfPoint(5,-4);echo ('<br>');
echo '<a href="index.php">return</a>';echo ('<br>');