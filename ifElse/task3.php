<?php

function sumOfPositive($a, $b, $c) {
    $sum = 0;
    if($a > 0) {
        $sum += $a;
    }
    if($b > 0) {
        $sum += $b;
    }
    if($c > 0) {
        $sum += $c;
    }
    return $sum;
}

echo sumOfPositive(1,2,3);echo ('<br>');
echo sumOfPositive(-5,2,3);echo ('<br>');
echo sumOfPositive(-5,2,-3);echo ('<br>');
echo '<a href="index.php">return</a>';echo ('<br>');